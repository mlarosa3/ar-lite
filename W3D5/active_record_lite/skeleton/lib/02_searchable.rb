require_relative 'db_connection'
require_relative '01_sql_object'

module Searchable
  def where(params)
    where_line = params.keys.map { |key| "#{key} = ?" }
    where_line = where_line.join(" AND ")
    where = DBConnection.instance.execute(<<-SQL, *params.values)
      SELECT
        *
      FROM
        #{table_name}
      WHERE
        (#{where_line})
    SQL

    where.map do |item|
      self.new(item)
    end
  end
end

class SQLObject
  extend Searchable
end
