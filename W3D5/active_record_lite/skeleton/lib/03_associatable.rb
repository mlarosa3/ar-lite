require_relative '02_searchable'
require 'active_support/inflector'

# Phase IIIa
class AssocOptions
  attr_accessor(
    :foreign_key,
    :class_name,
    :primary_key
  )

  def model_class
    @class_name.constantize
  end

  def table_name
    (@class_name += "s").downcase
  end
end

class BelongsToOptions < AssocOptions
  def initialize(name, options = {})
    defaults = {}
    defaults[:foreign_key] = (name.to_s + "_id").to_sym
    defaults[:primary_key] = :id
    defaults[:class_name]  = name.to_s.camelcase.singularize
    options = defaults.merge(options)
    @foreign_key = options[:foreign_key]
    @primary_key = options[:primary_key]
    @class_name  = options[:class_name]
  end
end

class HasManyOptions < AssocOptions
  def initialize(name, self_class_name, options = {})
    defaults = {}
    defaults[:foreign_key] = (self_class_name.to_s.downcase + "_id").to_sym
    defaults[:primary_key] = :id
    defaults[:class_name]  = name.to_s.camelcase.singularize
    options = defaults.merge(options)
    @foreign_key = options[:foreign_key]
    @primary_key = options[:primary_key]
    @class_name  = options[:class_name]
  end
end

module Associatable
  # Phase IIIb
  def belongs_to(name, options = {})
    self.assoc_options[name] = BelongsToOptions.new(name, options)
    define_method(name) do
      options = self.class.assoc_options[name]
      foreign_key_value = self.send(options.foreign_key)
      target_class = options.model_class
      target_class.where(:id => foreign_key_value).first
    end
  end

  def has_many(name, options = {})
    self.assoc_options[name] = HasManyOptions.new(name, self, options)
    define_method(name) do
      options = self.class.assoc_options[name]
      foreign_key_value = self.send(options.primary_key)
      target_class = options.model_class
      target_class.where(options.foreign_key => foreign_key_value)
    end
  end

  def assoc_options
    @assoc_opts ||= {}
    @assoc_opts
  end
end

class SQLObject
  extend Associatable
end
