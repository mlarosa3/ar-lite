require 'byebug'
require_relative 'db_connection'
require 'active_support/inflector'

class SQLObject
  def self.columns
    if @cols.nil?
      @cols = DBConnection.execute2(<<-SQL)
        SELECT
          *
        FROM
          #{table_name}
      SQL
    end
    @cols.first.map { |col| col.to_sym }
  end

  def self.finalize!
    columns.each do |col|
      define_method(col) do
        self.attributes
        @attributes[col.to_sym]
      end

      define_method("#{col}=") do |value|
        self.attributes
        @attributes[col.to_sym] = value
      end
    end
  end

  def self.table_name=(table_name)
    table_name.to_s.downcase + "s"
  end

  def self.table_name
    self.to_s.downcase + 's'
  end

  def self.all
    info = DBConnection.instance.execute(<<-SQL)
      SELECT
        *
      FROM
        #{table_name}
    SQL

    parse_all(info)
  end

  def self.parse_all(results)
    results.map do |result|
      self.new(result)
    end
  end

  def self.find(id)
    found = DBConnection.instance.execute(<<-SQL, id)
      SELECT
        *
      FROM
        #{table_name}
      WHERE
        id = ?
    SQL

    found.first ? self.new(found.first) : nil
  end

  def initialize(params = {})
    @cols = nil
    params.each do |attr_name,v|
      unless self.class.columns.include?(attr_name.to_sym)
        raise  "unknown attribute #{attr_name}"
      else
        name_as_symbol = "#{attr_name}=".to_sym
        self.send(name_as_symbol, v)
      end
    end
  end

  def attributes
    @attributes ||= {}
  end

  def attribute_values
    self.class.columns.map do |value|
      @attributes[value]
    end
  end

  def insert
    col_names = self.class.columns.map { |col| col.to_s }
    col_names = col_names.join(", ")
    values = self.attribute_values
    question_marks = "#{(["?"] * (values.count)).join(", ")}"
    DBConnection.instance.execute(<<-SQL, *values)
      INSERT INTO
        #{self.class.table_name} (#{col_names})
      VALUES
        (#{question_marks})
    SQL

    self.id = DBConnection.last_insert_row_id
  end

  def update
    col_names = self.class.columns.map { |col| "#{col} = ?" unless col == :id }
    col_names.delete_at(0)
    col_names = col_names.join(", ")
    values = self.attribute_values
    the_id = values.delete_at(0)
    question_marks = "#{(["?"] * (values.count)).join(", ")}"

    DBConnection.instance.execute(<<-SQL, *values, the_id)
      UPDATE
        #{self.class.table_name}
      SET
         #{col_names}
      WHERE
        id = ?
    SQL
  end

  def save
    if SQLObject.find(self.id)
      self.update
    else
      self.insert
    end
  end
end
